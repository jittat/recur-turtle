from turtle import *

stack = []

def push():
    stack.append([pos(), heading()])

def pop():
    p, h = stack[-1]
    del stack[-1]
    pu()
    setheading(h)
    setpos(p)
    pd()
    
def draw0(level, d):
    if level > 0:
        draw1(level-1, d)
        push()
        lt(45)
        draw0(level-1, d)
        pop()
        rt(45)
        draw0(level-1, d)
    else:
        fd(d)

def draw1(level, d):
    if level > 0:
        draw1(level-1, d)
        draw1(level-1, d)
    else:
        fd(d)

def init_turtle():
    hideturtle()
    up()
    bk(200)
    down()
    speed(0)
        
def main():
    init_turtle()

    draw0(6,5)
    done()

if __name__ == '__main__':
    main()
