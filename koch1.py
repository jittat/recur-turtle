from turtle import *

def edge(level, d):
    if level > 0:
        edge(level-1, d/2)
        lt(60)
        edge(level-1, d/2)
        rt(120)
        edge(level-1, d/2)
        lt(60)
        edge(level-1, d/2)
    else:
        fd(d)

def init_turtle():
    hideturtle()
    up()
    bk(200)
    down()
    speed(0)
        
def main():
    init_turtle()

    edge(5,64)
    done()

if __name__ == '__main__':
    main()
